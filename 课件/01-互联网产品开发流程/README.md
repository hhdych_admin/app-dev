# 互联网产品开发流程

### 课程内容

1. [产品的概念](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/01-%E4%BA%A7%E5%93%81%E7%9A%84%E6%A6%82%E5%BF%B5.md) 

2. [产品开发流程](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/02-%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md) 
   * 互联网公司组织结构
   * 产品与开发的区别
   * MVP模型

3. [用户需求挖掘](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/03-%E7%94%A8%E6%88%B7%E9%9C%80%E6%B1%82%E6%8C%96%E6%8E%98.md)
   * 用户痛点
   * 应用场景
     * 伪需求案例分析
     * 六何分析法
     * 案例分析《海岛赤脚》

4. [市场调研分析](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/04-%E5%B8%82%E5%9C%BA%E8%B0%83%E7%A0%94%E5%88%86%E6%9E%90.md)
   * 市场分析
     * 环境&趋势
     * 空间&规模
   * 竞品分析
     * 分析流程
   * 用户分析
     * 定性分析
       定量调研

5. [产品设计与规划](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/05-%E4%BA%A7%E5%93%81%E8%AE%BE%E8%AE%A1%E4%B8%8E%E8%A7%84%E5%88%92.md) 

   * 产品定位

   * 需求分析
   * 产品架构
   * 功能结构
   * 信息结构
   * 原型设计
     * 原型设计入门
     * 高保真原型
     * 原型设计实战
   * 功能说明
6. [产品需求评审](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/06-%E4%BA%A7%E5%93%81%E9%9C%80%E6%B1%82%E8%AF%84%E5%AE%A1.md) 
   * 案例分析《水滴_PRD_v1.0》