# 移动App开发简介

> 移动App一般是指在移动终端设备（一般指手机）上开发的软件或者服务。不同的移动终端设备所搭载的操作系统也不相同，而移动App开发与终端设备所搭载的系统息息相关，不同的系统下开发的环境、语言以及工具均不一样，接下来我们详细了解一下移动终端的操作系统。

### 操作系统

> 市面上移动终端设备搭载的系统大致有Android、iOS、OpenHarmony、Windows Phone、BlackBerry等，其中主流的是iOS、Android。

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106164426372.png" alt="image-20220106164426372" style="zoom:80%;" />

> 上图是国际数据公司IDC统计并公布的**全球智能手机出货量、市场份额同比数据**，其中2021年度搭载Android系统的手机有**14.9亿**台占全球市场总份额的**85.5%**，搭载iOS系统的有**2.5亿**台占全球市场总份额的**14.5%**。这两个操作系统已经将整个市场瓜分，由此可见，现阶段做移动App开发应该首选的是Android，因其占有全球市场的85.5%。

​                                               <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106170643873.png" alt="image-20220106170643873" style="zoom:50%;" />                                                    <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106171448877.png" alt="image-20220106171448877" style="zoom: 33%;" />                  

> 另外需要说明的是从19年开始国内涌出一支新秀Harmony OS操作系统，它是华为研发并应用在手机上基于OpenHarmony的一款商用版本，2020年华为手机的在国内的市场份额大概是38.3%，如果将华为的Android系统手机全部转换为Harmony OS系统，那么国内搭载Harmony OS的手机大概可以到2亿台，所以Harmony OS系统的App开发也不容小觑。

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106171628588.png" alt="image-20220106171628588" style="zoom:50%;" />

### 软件结构

> 一个软件如果把它拆分开来看的话，他的结构大概由两部分构成，分别是用户端（Client）和服务端（Server）。由多个Server构建出来的集群通过网络为各个用户端提供服务支撑，用户端可以是手机App软件、PC端软件、电视软件等等。

![image-20220106173009298](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106173009298.png)

#### 服务端（Server）

> 服务端一般也称之为后端。在信息处理系统中，通常由若干台计算机组成，提供数据和服务的机器，称之为服务端（Server）。

#### 用户端（Client）

> 用户端一般也称之为前端。向服务器提出请求数据和服务的计算机成为用户端（Client）。

#### 示例

> 下图是一个简单的登录功能业务流程图，首先是由用户点击进入了登录界面，然后输入用户名密码。当用户输入完毕用户名和密码后，进入服务端开始验证用户名密码是否正确，如果错误就返回给用户端的登录界面密码错误的提示，如果正确就继续判断用户的状态是否为封禁用户状态，如果是封禁状态就返回给用户端的登录界面提示封禁的信息，如果用户状态没有被封禁，就要继续判断是普通用户还是管理人员，如果是普通用户即通知客户端进入到用户界面中，如果是管理人员即通知客户端进入到管理界面中。

![image-20220109143715777](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220109143715777.png)

##### 产品功能的划分

1. 显示登录界面；【用户端】
2. 输入用户名和密码；【用户端】
3. 验证用户名密码是否正确？【服务端】
   * 密码错误：通知登录界面显示密码错误的提示；【服务端】
     * 在登录界面中提示密码错误；【用户端】
   * 密码正确：判断用户的状态是否为封禁用户？【服务端】
     * 封禁用户：通知登录界面显示封禁信息的提示；【服务端】
       * 在登录界面中提示账号被封禁；【用户端】
     * 正常用户：判断用户的权限是普通用户还是管理人员？【服务端】
       * 普通用户：通知登录界面切换到用户界面中；【服务端】
         * 显示用户界面；【用户端】
       * 管理人员：通知登录界面切换到管理界面中；【服务端】
         * 显示管理界面；【用户端】

##### 登录功能演示

> 在安装了JDK并且配置好JAVA_HOME以后，即可打开tomcat服务器，启动登录的服务端程序。详细操作如下：
>
> 1. 解压 [apache-tomcat-8.5.37.zip](./asserts/apache-tomcat-8.5.37.zip) 文件；
> 2. 在解压后的目录中找到`bin`目录；
> 3. 双击`bin` 目录下的 `startup.bat` 文件，启动服务；
>
> 启动成功后，会弹出一个命令行，如下图。

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220109171441648.png" alt="image-20220109171441648" style="zoom:70%;" />

> 当看到上图中的信息后，即表示登录的服务端程序启动成功，接下来我们使用浏览器作为用户端，打开用户端界面。打开浏览器，访问 http://localhost/login_server/login.html，显示信息如下图：

![image-20220109171808191](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220109171808191.png)

> 接下来输入用户名密码即可访问服务端的程序了，输入账户：itheima，密码：123321，点击确定，服务端会对用户端发送过来的账号和密码进行验证，并且将验证的结果反馈给用户端。

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220109172155242.png" alt="image-20220109172155242" style="zoom:67%;" />

> 服务端收到账号密码后，进行密码验证，这里服务端内置了3个账户及密码，如下：
>
> * 用户名：itheima，密码：123321
> * 用户名：zhangsan，密码：123456
> * 用户名：lisi，密码：456789
>
> 验证了itheima的账户密码后，将验证结果返回给用户端（浏览器）中，浏览器接收后显示如下：

![image-20220109172557433](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220109172557433.png)

> 这样一个完整的Client/Server流程就演示完了，示例中的用户端这里采用的是浏览器的方式。在移动App开发中我们的用户端一般不会用浏览器，更多的是用各平台下的本地(native)App，接下来进入到Android平台下的应用开发。
