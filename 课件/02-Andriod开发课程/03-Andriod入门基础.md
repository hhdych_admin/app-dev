# 建立第一个App

欢迎开始 Android 应用开发之旅！

本章节我们将学习如何建立我们的第一个Android应用程序。我们将学到如何创建一个Android项目和运行可调试版本的应用程序。此外，我们还将学习到一些Android应用程序设计的基础知识，包括如何创建一个简单的用户界面，以及处理用户输入。

开始本章节学习之前，我们要确保已经安装了开发环境。我们需要：

1. 下载[Android Studio](http://developer.android.com/sdk/index.html).

2. 使用[SDK Manager](http://developer.android.com/tools/help/sdk-manager.html)下载最新的SDK tools和platforms。

> Note：虽然这一系列的培训课程中的大多数章节都预期你会使用Android Studio来进行开发，但某些开发操作还是可以通过SDK tools中提供的命令来实现的。
>
> 本章节通过向导的方式来逐步创建一个小型的Android应用，通过这些步骤来教给我们一些Android开发的基本概念，因此你很有必要按照教程的步骤来学习操作。

## 创建 Android 项目

一个Android项目包含了所有构成Android应用的源代码文件。

本小节介绍如何使用Android Studio或者是SDK Tools中的命令行来创建一个新的项目。

> Note：在此之前，我们应该已经安装了Android SDK，如果使用Android Studio开发，应该确保已经安装了[Android Studio](http://developer.android.com/sdk/installing/studio.html)。否则，请先阅读 [Installing the Android SDK](http://developer.android.com/sdk/installing/index.html)按照向导完成安装步骤。

使用Android Studio创建项目

1. 使用Android Studio创建Android项目，启动Android Studio。

- 如果我们还没有用Android Studio打开过项目，会看到欢迎页，点击New Project。
- 如果已经用Android Studio打开过项目，点击菜单中的File，选择New Project来创建一个新的项目。


2. 参照图1在弹出的窗口（Configure your new project）中填入内容，点击Next。按照如图所示的值进行填写会使得后续的操作步骤不不容易差错。

- Application Name此处填写想呈现给用户的应用名称，此处我们使用“My First App”。
- Company domain 包名限定符，Android Studio会将这个限定符应用于每个新建的Android项目。
- Package Name是应用的包命名空间（同Java的包的概念），该包名在同一Android系统上所有已安装的应用中具有唯一性，我们可以独立地编辑该包名。
- Project location操作系统存放项目的目录。

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/studio-setup-1.png)

图1 Configure your new project

3. 在Select the form factors your app will run on窗口勾选Phone and Tablet。

4. Minimum SDK, 选择 API 8: Android 2.2 (Froyo). Minimum Required SDK表示我们的应用支持的最低Android版本，为了支持尽可能多的设备，我们应该设置为能支持你应用核心功能的最低API版本。如果某些非核心功能仅在较高版本的API支持，你可以只在支持这些功能的版本上开启它们(参考兼容不同的系统版本),此处采用默认值即可。

5. 不要勾选其他选项 (TV, Wear, and Glass) ，点击 Next.

6. 在Add an activity to <template> 窗口选择Blank Activity，点击 Next.

7. 在Choose options for your new file 窗口修改Activity Name 为MyActivity，修改 Layout Name 为activity_my，Title 修改为MyActivity，Menu Resource Name 修改为menu_my。

8. 点击Finish完成创建。

刚创建的Android项目是一个基础的Hello World项目，包含一些默认文件，我们花一点时间看看最重要的部分：

`app/src/main/res/layout/activity_my.xml`

这是刚才用Android Studio创建项目时新建的Activity对应的xml布局文件，按照创建新项目的流程，Android Studio会同时展示这个文件的文本视图和图形化预览视图，该文件包含一些默认设置和一个显示内容为“Hello world!”的TextView元素。

`app/src/main/java/com.mycompany.myfirstapp/MyActivity.java`

用Android Studio创建新项目完成后，可在Android Studio看到该文件对应的选项卡，选中该选项卡，可以看到刚创建的Activity类的定义。编译并运行该项目后，Activity启动并加载布局文件activity_my.xml，显示一条文本："Hello world!"

`app/src/main/AndroidManifest.xml`

manifest文件描述了项目的基本特征并列出了组成应用的各个组件，接下来的学习会更深入了解这个文件并添加更多组件到该文件中。

`app/build.gradle`

Android Studio使用Gradle 编译运行Android工程. 工程的每个模块以及整个工程都有一个build.gradle文件。通常你只需要关注模块的build.gradle文件，该文件存放编译依赖设置，包括defaultConfig设置：

- compiledSdkVersion 是我们的应用将要编译的目标Android版本，此处默认为你的SDK已安装的最新Android版本(目前应该是4.1或更高版本，如果你没有安装一个可用Android版本，就要先用SDK Manager来完成安装)，我们仍然可以使用较老的版本编译项目，但把该值设为最新版本，可以使用Android的最新特性，同时可以在最新的设备上优化应用来提高用户体验。
- applicationId 创建新项目时指定的包名。
- minSdkVersion 创建项目时指定的最低SDK版本，是新建应用支持的最低SDK版本。
- targetSdkVersion 表示你测试过你的应用支持的最高Android版本(同样用API level表示).当Android发布最新版本后，我们应该在最新版本的Android测试自己的应用同时更新target sdk到Android最新版本，以便充分利用Android新版本的特性。更多知识，请阅读Supporting Different Platform Versions。

更多关于Gradle的知识请阅读[Building Your Project with Gradle](http://developer.android.com/sdk/installing/studio-build.html)

注意/res目录下也包含了[resources](http://developer.android.com/guide/topics/resources/overview.html)资源：

`drawable<density>/`

存放各种densities图像的文件夹，mdpi，hdpi等，这里能够找到应用运行时的图标文件ic_launcher.png

`layout/`

存放用户界面文件，如前边提到的activity_my.xml，描述了MyActivity对应的用户界面。

`menu/`

存放应用里定义菜单项的文件。

`values/`

存放其他xml资源文件，如string，color定义。string.xml定义了运行应用时显示的文本"Hello world!"

要运行这个APP，继续下个小节的学习。

使用命令行创建项目
如果没有使用Android Studio开发Android项目，我们可以在命令行使用SDK提供的tools来创建一个Android项目。

1. 打开命令行切换到SDK根目录下；

2. 执行:

`tools/android list targets`

会在屏幕上打印出我们所有的Android SDK中下载好的可用Android platforms，找想要创建项目的目标platform，记录该platform对应的Id，推荐使用最新的platform。我们仍可以使自己的应用支持较老版本的platform，但设置为最新版本允许我们为最新的Android设备优化我们的应用。 如果没有看到任何可用的platform，我们需要使用Android SDK Manager完成下载安装，参见 Adding Platforms and Packages。

3. 执行：

```
android create project --target <target-id> --name MyFirstApp \
--path <path-to-workspace>/MyFirstApp --activity MyActivity \
--package com.example.myfirstapp
```

替换<target-id>为上一步记录好的Id，替换<path-to-workspace>为我们想要保存项目的路径。

Tip:把platform-tools/和 tools/添加到环境变量PATH，开发更方便。

到此为止，我们的Android项目已经是一个基本的“Hello World”程序，包含了一些默认的文件。要运行它，继续下个小节的学习。

## 执行Android程序

通过上一节课创建了一个Android的Hello World项目，项目默认包含一系列源文件，它让我们可以立即运行应用程序。

如何运行Android应用取决于两件事情：是否有一个Android设备和是否正在使用Android Studio开发程序。本节课将会教使用Android Studio和命令行两种方式在真实的android设备或者android模拟器上安装并且运行应用。

 **在真实设备上运行** 

如果有一个真实的Android设备，以下的步骤可以使我们在自己的设备上安装和运行应用程序：

手机设置

把设备用USB线连接到计算机上。如果是在windows系统上进行开发的，你可能还需要安装你设备对应的USB驱动，详见OEM USB Drivers 文档。
开启设备上的USB调试选项。
在大部分运行Andriod3.2或更老版本系统的设备上，这个选项位于“设置>应用程序>开发选项”里。

在Andriod 4.0或更新版本中，这个选项在“设置>开发人员选项”里。

> Note: 从Android4.2开始，开发人员选项在默认情况下是隐藏的，想让它可见，可以去设置>关于手机（或者关于设备)点击版本号七次。再返回就能找到开发人员选项了。

 **从Android Studio运行程序** 

1. 选择项目的一个文件，点击工具栏里的Run按钮。

从命令行安装运行应用程序

打开命令行并切换当前目录到Andriod项目的根目录，在debug模式下使用Gradle编译项目，使用gradle脚本执行assembleDebug编译项目，执行后会在build/目录下生成MyFirstApp-debug.apk。

Windows操作系统下，执行：

`gradlew.bat assembleDebug`

Mac OS或Linux系统下：

```
$ chmod +x gradlew
$ ./gradlew assembleDebug
```

编译完成后在app/build/outputs/apk/目录生成apk。

`Note: chmod命令是给gradlew增加执行权限，只需要执行一次。`

确保 Android SDK里的 platform-tools/ 路径已经添加到环境变量PATH中，执行：

`adb install bin/MyFirstApp-debug.apk`

在我们的Android设备中找到 MyFirstActivity，点击打开。

 **在模拟器上运行** 

无论是使用 Android Studio 还是命令行，在模拟器中运行程序首先要创建一个 Android Virtual Device (AVD)。AVD 是对 Android 模拟器的配置，可以让我们模拟不同的设备。

创建一个 AVD:

- 启动 Android Virtual Device Manager（AVD Manager）的两种方式：
- 用Android Studio, Tools > Android > AVD Manager,或者点击工具栏里面Android Virtual Device Managerimage
- ；
- 在命令行窗口中，把当前目录切换到<sdk>/tools/ 后执行：

android avd

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/andriod-avd.png)

2 在AVD Manager 面板中，点击Create Virtual Device.

3 在Select Hardware窗口，选择一个设备，比如 Nexus 6，点击Next。

4 选择列出的合适系统镜像.

5 校验模拟器配置，点击Finish。

## 建立简单的用户界面

在本小节里，我们将学习如何用XML创建一个带有文本输入框和按钮的界面，下一节课将学会使app对按钮做出响应：按钮被按下时，文本框里的内容被发送到另外一个Activity。

Android的图形用户界面是由多个View和ViewGroup构建出来的。View是通用的UI窗体小组件，比如按钮(Button)或者文本框(text field)，而ViewGroup是不可见的，是用于定义子View布局方式的容器，比如网格部件(grid)和垂直列表部件(list)。

Android提供了一个对应于View和ViewGroup子类的一系列XMl标签，我们可以在XML里使用层级视图元素创建自己的UI。

Layouts是ViewGroup的子类，接下来的练习将使用LinearLayout。

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-View.png)

`可选的布局文件：在XML中定义界面布局而不是在运行时去动态生成布局是有多个原因的，其中最重要的一点是这样可以使得你为不同大小的屏幕创建不同的布局文件。例如，你可以创建创建2个版本的布局文件，告诉系统在小的屏幕上使用其中一个布局文件，在大的屏幕上使用另外一个布局文件。更多信息，请参考兼容不同的设备`

 **创建一个LinearLayout** 

1 在Android Studio中，从res/layout目录打开activity_my.xml文件。上一节创建新项目时生成的BlankActivity，包含一个activity_my.xml文件，该文件根元素是一个包含TextView的RelativeLayout。

2 在Preview面板点击image
关闭右侧Preview面板，在Android Studio中，当打开布局文件的时，可以看到一个Preview面板，点击这个面板中的标签，可利用WYSIWYG（所见即所得）工具在Design面板看到对应的图形化效果，但在本节直接操作XML文件即可。

3 删除 TextView 标签.

4 把 RelativeLayout 标签改为 LinearLayout.

5 为添加 android:orientation 属性并设置值为 "horizontal".

6 去掉android:padding 属性和tools:context 属性.

修改后结果如下：

res/layout/activity_my.xml

```
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal" >
</LinearLayout>
```
LinearLayout是ViewGroup的一个子类，用于放置水平或者垂直方向的子视图部件，放置方向由属性android:orientation设定。LinearLayout里的子布局按照XML里定义的顺序显示在屏幕上。

所有的Views都需要用到android:layout_width和android:layout_height这两个属性来设置自身的大小。

由于LinearLayout是整个视图的根布局，所以其宽和高都应充满整个屏幕的，通过指定width 和 height属性为"match_parent"。该值表示子View扩张自己width和height来匹配父控件的width和height。

更多关于Layout属性的信息，请参照XML布局向导。

 **添加一个文本输入框** 

与其它View一样，我们需要设置XML里的某些属性来指定EditText的属性值，以下是应该在线性布局里指定的一些属性元素：

1 在activity_my.xml文件的 标签内定义一个 标签，并设置id属性为@+id/edit_message.

2 设置layout_width和layout_height属性为 wrap_content.

3 设置hint属性为一个string 值的引用edit_message.

代码如下：

res/layout/activity_my.xml

```
<EditText android:id="@+id/edit_message"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:hint="@string/edit_message" />
```

各属性说明:

android:id
这是定义View的唯一标识符。可以在程序代码中通过该标识符对对象进行引用，例如对这个对象进行读和修改的操作(在下一课里将会用到)。

当想从XML里引用资源对象的时候必须使用@符号。紧随@之后的是资源的类型(这里是id)，然后是资源的名字(这里使用的是edit_message)。

+号只是当你第一次定义一个资源ID的时候需要。这里是告诉SDK此资源ID需要被创建出来。在应用程序被编译之后，SDK就可以直接使用ID值，edit_message是在项目gen/R.java文件中创建一个新的标识符，这个标识符就和EditText关联起来了。一旦资源ID被创建了，其他资源如果引用这个ID就不再需要+号了。这里是唯一一个需要+号的属性。

android:layout_width 和android:layout_height
对于宽和高不建议指定具体的大小，使用wrap_content指定之后，这个视图将只占据内容大小的空间。如果你使用了match_parent，这时EditText将会布满整个屏幕，因为它将适应父布局的大小。更多信息，请参考 布局向导。

android:hint
当文本框为空的时候,会默认显示这个字符串。对于字符串@string/edit_message的值所引用的资源应该是定义在单独的文件里，而不是直接使用字符串。因为使用的值是存在的资源，所以不需要使用+号。然而，由于你还没有定义字符串的值，所以在添加@string/edit_message时候会出现编译错误。下边你可以定义字符串资源值来去除这个错误。

Note: 该字符串资源与id使用了相同的名称（edit_message）。然而，对于资源的引用是区分类型的（比如id和字符串），因此，使用相同的名称不会引起冲突。

增加字符串资源
默认情况下，你的Android项目包含一个字符串资源文件，res/values/string.xml。打开这个文件，为"edit_message"增加一个供使用的字符串定义，设置值为"Enter a message."

1 在Android Studio里，编辑 res/values 下的 strings.xml 文件.

2 添加一个string名为"edit_message" ,值为 "Enter a message".

3 再添加一个string名为 "button_send"，值为"Send".下面的内容将使用这个string来创建一个按钮.

4 删除 "hello world" string这一行.

下边就是修改好的res/values/strings.xml：

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="app_name">My First App</string>
    <string name="edit_message">Enter a message</string>
    <string name="button_send">Send</string>
    <string name="action_settings">Settings</string>
    <string name="title_activity_main">MainActivity</string>
</resources>
```

当你在用户界面定义一个文本的时候，你应该把每一个文本字符串列入资源文件。这样做的好处是：对于所有字符串值，字符串资源能够单独的修改，在资源文件里你可以很容易的找到并且做出相应的修改。通过选择定义每个字符串，还允许您对不同语言本地化应用程序。

更多的于不同语言本字符串资源本地化的问题，请参考兼容不同的设备(Supporting Different Devices) 。

 **添加一个按钮** 

1 在 Android Studio里, 编辑 res/layout下的 activity_my.xml 文件.

2 在LinearLayout 内部, 在标签之后定义一个标签.

3 设置Button的width 和 height 属性值为 "wrap_content" 以便让Button大小能完整显示其上的文本.

4 定义button的文本使用android:text 属性，设置其值为之前定义好的 button_send 字符串.

此时的 LinearLayout 看起来应该是这样

res/layout/activity_my.xml

```
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal" >
      <EditText android:id="@+id/edit_message"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:hint="@string/edit_message" />
      <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/button_send" />
</LinearLayout>
```

Note 宽和高被设置为"wrap_content"，这时按钮占据的大小就是按钮里文本的大小。这个按钮不需要指定android:id的属性，因为Activity代码中不会引用该Button。

当前EditText和Button部件只是适应了他们各自内容的大小，如下图所示：

`edittext_wrap`

这样设置对按钮来说很合适，但是对于文本框来说就不太好了，因为用户可能输入更长的文本内容。因此如果能够占满整个屏幕宽度会更好。LinearLayout使用_权重_属性来达到这个目的，你可以使用android:layout_weight属性来设置。

权重的值指的是每个部件所占剩余空间的大小，该值与同级部件所占空间大小有关。就类似于饮料的成分配方：“两份伏特加酒，一份咖啡利口酒”，即该酒中伏特加酒占三分之二。例如，我们设置一个View的权重是2，另一个View的权重是1，那么总数就是3，这时第一个View占据2/3的空间，第二个占据1/3的空间。如果你再加入第三个View，权重设为1，那么第一个View(权重为2的)会占据1/2的空间，剩余的另外两个View各占1/4。(请注意，使用权重的前提一般是给View的宽或者高的大小设置为0dp，然后系统根据上面的权重规则来计算View应该占据的空间。但是很多情况下，如果给View设置了match_parent的属性，那么上面计算权重时则不是通常的正比，而是反比，也就是权重值大的反而占据空间小)。

对于所有的View默认的权重是0，如果只设置了一个View的权重大于0，则该View将占据除去别的View本身占据的空间的所有剩余空间。因此这里设置EditText的权重为1，使其能够占据除了按钮之外的所有空间。

 **让输入框充满整个屏幕的宽度** 

为让 EditText充满剩余空间，做如下操作：

1 在activity_my.xml文件里，设置EditText的layout_weight属性值为1 .

2 设置EditText的layout_width值为0dp.

res/layout/activity_my.xml

```
<EditText
    android:layout_weight="1"
    android:layout_width="0dp"
    ... />
```

为了提升布局的效率，在设置权重的时候，应该把EditText的宽度设为0dp。如果设置"wrap_content"作为宽度，系统需要自己去计算这个部件所占有的宽度，而此时的因为设置了权重，所以系统自动会占据剩余空间，EditText的宽度最终成了不起作用的属性。

设置权重后的效果图

`edittext_gravity`

现在看一下完整的布局文件内容：

res/layout/activity_my.xml

```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal">
    <EditText android:id="@+id/edit_message"
        android:layout_weight="1"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:hint="@string/edit_message" />
    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/button_send" />
</LinearLayout>
```

 **运行应用** 

整个布局默认被应用于创建项目的时候SDK工具自动生成的Activity，运行看下效果:

在Android Studio里，点击工具栏里的Run按钮

或者使用命令行，进入你项目的根目录直接执行

```
ant debug
adb install bin/MyFirstApp-debug.apk
```
## 启动其他的Activity

在完成上一课(建立简单的用户界面)后，我们已经拥有了显示一个activity（一个界面）的app（应用），该activity包含了一个文本字段和一个按钮。在这节课中，我们将添加一些新的代码到MyActivity中，当用户点击发送(Send)按钮时启动一个新的activity。

 **响应Send(发送)按钮** 

1 在Android Studio中打开res/layout目录下的activity_my.xml 文件.

2 为 Button 标签添加android:onclick属性.

res/layout/activity_my.xml

```
<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="@string/button_send"
    android:onClick="sendMessage" />
```

android:onclick属性的值"sendMessage"即为用户点击屏幕按钮时触发方法的名字。

3 打开java/com.mycompany.myfirstapp目录下MyActivity.java 文件.

4 在MyActivity.java 中添加sendMessage() 函数：

java/com.mycompany.myfirstapp/MyActivity.java

/** Called when the user clicks the Send button */
public void sendMessage(View view) {
    // Do something in response to button
}

为使系统能够将该方法（你刚在MyActivity.java中添加的sendMessage方法）与在android:onClick属性中提供的方法名字匹配，它们的名字必须一致，特别需要注意的是，这个方法必须满足以下条件：

- 是public函数
- 无返回值
- 参数唯一(为View类型,代表被点击的视图）
- 接下来，你可以在这个方法中编写读取文本内容，并将该内容传到另一个Activity的代码。

 **构建一个Intent** 

Intent是在不同组件中(比如两个Activity)提供运行时绑定的对象。Intent代表一个应用"想去做什么事"，你可以用它做各种各样的任务，不过大部分的时候他们被用来启动另一个Activity。更详细的内容可以参考Intents and Intent Filters。

1 在MyActivity.java的sendMessage()方法中创建一个Intent并启动名为DisplayMessageActivity的Activity：

java/com.mycompany.myfirstapp/MyActivity.java

Intent intent = new Intent(this, DisplayMessageActivity.class);
Note：如果使用的是类似Android Studio的IDE，这里对DisplayMessageActivity的引用会报错，因为这个类还不存在；暂时先忽略这个错误，我们很快就要去创建这个类了。

在这个Intent构造函数中有两个参数：

第一个参数是Context(之所以用this是因为当前Activity是Context的子类)

接受系统发送Intent的应用组件的Class（在这个案例中，指将要被启动的activity）。

Android Studio会提示导入Intent类。

2 在文件开始处导入Intent类:

java/com.mycompany.myfirstapp/MyActivity.java

import android.content.Intent;
**Tip:**在Android Studio中，按Alt + Enter 可以导入缺失的类(在Mac中使用option + return)

3 在sendMessage()方法里用findViewById()方法得到EditText元素.

java/com.mycompany.myfirstapp/MyActivity.java

public void sendMessage(View view) {
  Intent intent = new Intent(this, DisplayMessageActivity.class);
  EditText editText = (EditText) findViewById(R.id.edit_message);
}

4 在文件开始处导入EditText类.

在Android Studio中，按Alt + Enter 可以导入缺失的类(在Mac中使用option + return)

5 把EditText的文本内容关联到一个本地 message 变量，并使用putExtra()方法把值传给intent.

java/com.mycompany.myfirstapp/MyActivity.java

public void sendMessage(View view) {
  Intent intent = new Intent(this, DisplayMessageActivity.class);
  EditText editText = (EditText) findViewById(R.id.edit_message);
  String message = editText.getText().toString();
  intent.putExtra(EXTRA_MESSAGE, message);
}
Intent可以携带称作 extras 的键-值对数据类型。 [putExtra()](http://developer.android.com/reference/android/content/Intent.html#putExtra(java.lang.String, android.os.Bundle))方法把键名作为第一个参数，把值作为第二个参数。

6 在MyActivity class,定义EXTRA_MESSAGE :

java/com.mycompany.myfirstapp/MyActivity.java


```
public class MyActivity extends ActionBarActivity {
    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";
    ...
}
```

为让新启动的activity能查询extra数据。定义key为一个public型的常量，通常使用应用程序包名作为前缀来定义键是很好的做法，这样在应用程序与其他应用程序进行交互时仍可以确保键是唯一的。

7 在sendMessage()函数里，调用startActivity()完成新activity的启动，现在完整的代码应该是下面这个样子：

java/com.mycompany.myfirstapp/MyActivity.java


```
/** Called when the user clicks the Send button */
public void sendMessage(View view) {
    Intent intent = new Intent(this, DisplayMessageActivity.class);
    EditText editText = (EditText) findViewById(R.id.edit_message);
    String message = editText.getText().toString();
    intent.putExtra(EXTRA_MESSAGE, message);
    startActivity(intent);
}
```

运行这个方法，系统收到我们的请求后会实例化在Intent中指定的Activity，现在需要创建一个DisplayMessageActivity类使程序能够执行起来。

 **创建第二个Activity** 

Activity所有子类都必须实现onCreate()方法。创建activity的实例时系统会调用该方式，此时必须用 setContentView()来定义Activity布局，以对Activity进行初始化。


- 使用Android Studio创建新的Activity
- 使用Android Studio创建的activity会实现一个默认的onCreate()方法.


在Android Studio的java 目录, 选择包名 com.mycompany.myfirstapp,右键选择 New > Activity > Blank Activity.

在Choose options窗口，配置activity：

- Activity Name: DisplayMessageActivity
- Layout Name: activity_display_message

Title: My Message

Hierarchical Parent: com.mycompany.myfirstapp.MyActivity

Package name: com.mycompany.myfirstapp点击 Finish.

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-Activity.png)

3 打开DisplayMessageActivity.java文件，此类已经实现了onCreate()方法，稍后需要更新此方法。另外还有一个onOptionsItemSelected()方法，用来处理action bar的点击行为。暂时保留这两个方法不变。

4 由于这个应用程序并不需要onCreateOptionsMenu()，直接删除这个方法。

如果使用 Android Studio开发，现在已经可以点击Send按钮启动这个activity了，但显示的仍然是模板提供的默认内容"Hello world"，稍后修改显示自定义的文本内容。

使用命令行创建activity

如果使用命令行工具创建activity，按如下步骤操作：

1 在工程的src/目录下，紧挨着MyActivity.java创建一个新文件DisplayMessageActivity.java.

2 写入如下代码：

```
public class DisplayMessageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() { }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                  Bundle savedInstanceState) {
              View rootView = inflater.inflate(R.layout.fragment_display_message,
                      container, false);
              return rootView;
        }
    }
}
```

Note:如果使用的IDE不是 Android Studio，工程中可能不会包含由setContentView()请求的activity_display_message layout，但这没关系，因为等下会修改这个方法。

3 把新Activity的标题添加到strings.xml文件:

```
<resources>
    ...
    <string name="title_activity_display_message">My Message</string>
</resources>
```

4 在 AndroidManifest.xml的Application 标签内为 DisplayMessageActivity添加 标签，如下:

```
<application ... >
    ...
    <activity
        android:name="com.mycompany.myfirstapp.DisplayMessageActivity"
        android:label="@string/title_activity_display_message"
        android:parentActivityName="com.mycompany.myfirstapp.MyActivity" >
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.mycompany.myfirstapp.MyActivity" />
    </activity>
</application>
```

android:parentActivityName属性声明了在应用程序中该Activity逻辑层面的父类Activity的名称。 系统使用此值来实现默认导航操作，比如在Android 4.1（API level 16）或者更高版本中的Up navigation。 使用Support Library，如上所示的<meta-data>元素可以为安卓旧版本提供相同功能。

Note:我们的Android SDK应该已经包含了最新的Android Support Library，它包含在ADT插件中。但如果用的是别的IDE，则需要在 Adding Platforms and Packages 中安装。当Android Studio中使用模板时，Support Library会自动加入我们的工程中(在Android Dependencies中你以看到相应的JAR文件)。如果不使用Android Studio，就需要手动将Support Library添加到我们的工程中，参考setting up the Support Library。

 **接收Intent** 

不管用户导航到哪，每个Activity都是通过Intent被调用的。我们可以通过调用getIntent()来获取启动activity的Intent及其包含的数据。

1 编辑java/com.mycompany.myfirstapp目录下的DisplayMessageActivity.java文件.

2 删除onCreate()方法中下面一行:

`  setContentView(R.layout.activity_display_message);`
3 得到intent 并赋值给本地变量.

`Intent intent = getIntent();`

4 为Intent导入包.

在Android Studio中，按Alt + Enter 可以导入缺失的类(在Mac中使用option + return).

5 调用 getStringExtra()提取从 MyActivity 传递过来的消息.

`String message = intent.getStringExtra(MyActivity.EXTRA_MESSAGE);`

显示文本

1 在onCreate() 方法中, 创建一个 TextView 对象.

TextView textView = new TextView(this);

2 设置文本字体大小和内容.

```
textView.setTextSize(40);
textView.setText(message);
```

3 通过调用activity的setContentView()把TextView作为activity布局的根视图.

setContentView(textView);

4 为TextView 导入包.

在Android Studio中，按Alt + Enter 可以导入缺失的类(在Mac中使用option + return).

DisplayMessageActivity的完整onCreate()方法应该如下：

```
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Get the message from the intent
    Intent intent = getIntent();
    String message = intent.getStringExtra(MyActivity.EXTRA_MESSAGE);

    // Create the text view
    TextView textView = new TextView(this);
    textView.setTextSize(40);
    textView.setText(message);

    // Set the text view as the activity layout
    setContentView(textView);
}
```
现在你可以运行app，在文本中输入信息，点击Send(发送)按钮，ok，现在就可以在第二Activity上看到发送过来信息了。如图：

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-HelloWorld.png)

# 添加ActionBar

Action Bar是我们可以为activity实现的最重要的设计元素之一。其提供了多种 UI 特性，可以让我们的 app 与其他 Android app 保持较高的一致性，从而为用户所熟悉。核心的功能包括：

- 一个专门的空间用来显示你的app的标识，以及指出目前所处在app的哪个页面。
- 以一种可预见的方式访问重要的操作（比如搜索）。
- 支持导航和视图切换（通过Tabs和下拉列表）

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-ActionBar.png)

## 建立ActionBar

Action bar 最基本的形式，就是为 Activity 显示标题，并且在标题左边显示一个 app icon。即使在这样简单的形式下，action bar对于所有的 activity 来说是十分有用的。它告知用户他们当前所处的位置，并为你的 app 维护了持续的同一标识。

设置一个基本的 action bar，需要 app 使用一个 activity 主题，该主题必须是 action bar 可用的。如何声明这样的主题取决于我们 app 支持的 Android 最低版本。本课程根据我们 app 支持的 Android 最低版本分为两部分。

仅支持 Android 3.0 及以上版本

从 Android 3.0(API lever 11) 开始，所有使用 Theme.Holo 主题（或者它的子类）的 Activity 都包含了 action bar，当 targetSdkVersion 或 minSdkVersion 属性被设置成 “11” 或更大时，它是默认主题。

所以，要为 activity 添加 action bar，只需简单地设置属性为 11 或者更大。例如：


```
<manifest ... >
    <uses-sdk android:minSdkVersion="11" ... />
    ...
</manifest>
```

注意: 如果创建了一个自定义主题，需确保这个主题使用一个 Theme.Holo的主题作为父类。详情见 Action bar 的风格化

到此，我们的 app 使用了 Theme.Holo 主题，并且所有的 activity 都显示 action bar。

支持 Android 2.1 及以上版本
当 app 运行在 Andriod 3.0 以下版本（不低于 Android 2.1）时，如果要添加 action bar，需要加载 Android Support 库。

开始之前，通过阅读Support Library Setup文档来建立v7 appcompat library（下载完library包之后，按照Adding libraries with resources的指引进行操作）。

在 Support Library集成到你的 app 工程中之后：

1、更新 activity，以便于它继承于 ActionBarActivity。例如：

`public class MainActivity extends ActionBarActivity { ... }`

2、在 mainfest 文件中，更新 <application> 标签或者单一的 <activity> 标签来使用一个 Theme.AppCompat 主题。例如：

<activity android:theme="@style/Theme.AppCompat.Light" ... >
注意: 如果创建一个自定义主题，需确保其使用一个 Theme.AppCompat 主题作为父类。详情见 Action bar 风格化

现在，当 app 运行在 Android 2.1(API level 7) 或者以上时，activity 将包含 action bar。

切记，在 manifest 中正确地设置 app 支持的 API level：

```
<manifest ... >
    <uses-sdk android:minSdkVersion="7"  android:targetSdkVersion="18" />
    ...
</manifest>
```
## 添加Action按钮

Action bar 允许我们为当前环境下最重要的操作添加按钮。那些直接出现在 action bar 中的 icon 和/或文本被称作action buttons(操作按钮)。安排不下的或不足够重要的操作被隐藏在 action overflow 

在 XML 中指定操作
所有的操作按钮和 action overflow 中其他可用的条目都被定义在 menu资源 的 XML 文件中。通过在项目的 res/menu 目录中新增一个 XML 文件来为 action bar 添加操作。

为想要添加到 action bar 中的每个条目添加一个 <item> 元素。例如：

res/menu/main_activity_actions.xml


```
<menu xmlns:android="http://schemas.android.com/apk/res/android" >
    <!-- 搜索, 应该作为动作按钮展示-->
    <item android:id="@+id/action_search"
          android:icon="@drawable/ic_action_search"
          android:title="@string/action_search"
          android:showAsAction="ifRoom" />
    <!-- 设置, 在溢出菜单中展示 -->
    <item android:id="@+id/action_settings"
          android:title="@string/action_settings"
          android:showAsAction="never" />
</menu>
```

上述代码声明，当 action bar 有可用空间时，搜索操作将作为一个操作按钮来显示，但设置操作将一直只在 action overflow 中显示。（默认情况下，所有的操作都显示在 action overflow 中，但为每一个操作指明设计意图是很好的做法。）

icon 属性要求每张图片提供一个 resource ID。在 @drawable/ 之后的名字必须是存储在项目目录 res/drawable/ 下位图图片的文件名。例如：ic_action_search.png 对应 "@drawable/ic_action_search"。同样地，title 属性使用通过 XML 文件定义在项目目录 res/values/ 中的一个 string 资源，详情请参见 创建一个简单的 UI 。

注意：当创建 icon 和其他 bitmap 图片时，要为不同屏幕密度下的显示效果提供多个优化的版本，这一点很重要。在 支持不同屏幕 课程中将会更详细地讨论。

如果为了兼容 Android 2.1 的版本使用了 Support 库，在 android 命名空间下 showAsAction 属性是不可用的。Support 库会提供替代它的属性，我们必须声明自己的 XML 命名空间，并且使用该命名空间作为属性前缀。（一个自定义 XML 命名空间需要以我们的 app 名称为基础，但是可以取任何想要的名称，它的作用域仅仅在我们声明的文件之内。）例如：

res/menu/main_activity_actions.xml


```
<menu xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:yourapp="http://schemas.android.com/apk/res-auto" >
    <!-- 搜索, 应该展示为动作按钮 -->
    <item android:id="@+id/action_search"
          android:icon="@drawable/ic_action_search"
          android:title="@string/action_search"
          yourapp:showAsAction="ifRoom"  />
    ...
</menu>
```

为 Action Bar 添加操作
要为 action bar 布局菜单条目，就要在 activity 中实现 onCreateOptionsMenu() 回调方法来 inflate 菜单资源从而获取 Menu 对象。例如：


```
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    // 为ActionBar扩展菜单项
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_activity_actions, menu);
    return super.onCreateOptionsMenu(menu);
}
```

为操作按钮添加响应事件
当用户按下某一个操作按钮或者 action overflow 中的其他条目，系统将调用 activity 中onOptionsItemSelected()的回调方法。在该方法的实现里面调用MenuItem的getItemId()来判断哪个条目被按下 - 返回的 ID 会匹配我们声明对应的 <item> 元素中 android:id 属性的值。

```
@Override
public boolean onOptionsItemSelected(MenuItem item) {
    // 处理动作按钮的点击事件
    switch (item.getItemId()) {
        case R.id.action_search:
            openSearch();
            return true;
        case R.id.action_settings:
            openSettings();
            return true;
        default:
            return super.onOptionsItemSelected(item);
    }
}
```

为下级 Activity 添加向上按钮
在不是程序入口的其他所有屏中（activity 不位于主屏时），需要在 action bar 中为用户提供一个导航到逻辑父屏的up button(向上按钮)。

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-Mail.png)

Gmail 中的 up button。

当运行在 Android 4.1(API level 16) 或更高版本，或者使用 Support 库中的 ActionBarActivity 时，实现向上导航需要在 manifest 文件中声明父 activity ，同时在 action bar 中设置向上按钮可用。

如何在 manifest 中声明一个 activity 的父类，例如：


```
<application ... >
    ...
    <!-- 主 main/home 活动 (没有上级活动) -->
    <activity
        android:name="com.example.myfirstapp.MainActivity" ...>
        ...
    </activity>
    <!-- 主活动的一个子活动-->
    <activity
        android:name="com.example.myfirstapp.DisplayMessageActivity"
        android:label="@string/title_activity_display_message"
        android:parentActivityName="com.example.myfirstapp.MainActivity" >
        <!--  meta-data 用于支持 support 4.0 以及以下来指明上级活动 -->
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.example.myfirstapp.MainActivity" />
    </activity>
</application>
```

然后，通过调用setDisplayHomeAsUpEnabled() 来把 app icon 设置成可用的向上按钮：

```
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_displaymessage);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    // 如果你的minSdkVersion属性是11活更高, 应该这么用:
    // getActionBar().setDisplayHomeAsUpEnabled(true);
}
```

由于系统已经知道 MainActivity 是 DisplayMessageActivity 的父 activity，当用户按下向上按钮时，系统会导航到恰当的父 activity - 你不需要去处理向上按钮的事件。

更多关于向上导航的信息，请见 提供向上导航。

## 自定义ActionBar的风格

Action bar 为用户提供一种熟悉可预测的方式来展示操作和导航，但是这并不意味着我们的 app 要看起来和其他 app 一样。如果想将 action bar 的风格设计的合乎我们产品的定位，只需简单地使用 Android 的 样式和主题 资源。

Android 包括一少部分内置的 activity 主题，这些主题中包含 “dark” 或 “light” 的 action bar 样式。我们也可以扩展这些主题，以便于更好的为 action bar 自定义外观。

注意：如果我们为 action bar 使用了 Support 库的 API，那我们必须使用（或重写） Theme.AppCompat 家族样式（甚至 Theme.Holo 家族，在 API level 11 或更高版本中可用）。如此一来，声明的每一个样式属性都必须被声明两次：一次使用系统平台的样式属性（android: 属性），另一次使用 Support 库中的样式属性（appcompat.R.attr 属性 - 这些属性的上下文其实就是我们的 app）。更多细节请查看下面的示例。

 **使用一个 Android 主题** 

Android 包含两个基本的 activity 主题，这两个主题决定了 action bar 的颜色：

- Theme.Holo，一个 “dark” 的主题
- Theme.Holo.Light，一个 “light” 的主题

这些主题即可以被应用到 app 全局，也可以通过在 manifest 文件中设置 <application> 元素 或 <activity> 元素的 android:theme 属性，对单一的 activity 进行设置。

例如：

`<application android:theme="@android:style/Theme.Holo.Light" ... />`

可以通过声明 activity 的主题为 Theme.Holo.Light.DarkActionBar 来达到如下效果：action bar 为dark，其他部分为light。

actionbar-theme-light-darkactionbar@2x.png

当使用 Support 库时，必须使用 Theme.AppCompat 主题替代：


- Theme.AppCompat，一个“dark”的主题
- Theme.AppCompat.Light，一个“light”的主题
- Theme.AppCompat.Light.DarkActionBar，一个带有“dark” action bar 的“light”主题

一定要确保我们使用的 action bar icon 的颜色与 action bar 本身的颜色有差异。Action Bar Icon Pack 为 Holo “dark”和“light”的 action bar 提供了标准的 action icon。

 **自定义背景** 

为改变 action bar的背景，可以通过为 activity 创建一个自定义主题，并重写 actionBarStyle 属性来实现。actionBarStyle 属性指向另一个样式；在该样式里，通过指定一个 drawable 资源来重写 background 属性。

actionbar-theme-custom@2x.png

如果我们的 app 使用了 navigation tabs 或 split action bar ，也可以通过分别设置 backgroundStacked 和 backgroundSplit 属性来为这些条指定背景。

Note：为自定义主题和样式声明一个合适的父主题，这点很重要。如果没有父样式，action bar将会失去很多默认的样式属性，除非我们自己显式的对他们进行声明。

仅支持 Android 3.0 和更高
当仅支持 Android 3.0 和更高版本时，可以通过如下方式定义 action bar 的背景：

res/values/themes.xml

```

<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- 应用于程序或者活动的主题 -->
    <style name="CustomActionBarTheme"
           parent="@android:style/Theme.Holo.Light.DarkActionBar">
        <item name="android:actionBarStyle">@style/MyActionBar</item>
    </style>

    <!-- ActionBar 样式 -->
    <style name="MyActionBar"
           parent="@android:style/Widget.Holo.Light.ActionBar.Solid.Inverse">
        <item name="android:background">@drawable/actionbar_background</item>
    </style>
</resources>
```

然后，将主题应用到 app 全局或单个的 activity 之中：

<application android:theme="@style/CustomActionBarTheme" ... />
支持 Android 2.1 和更高
当使用 Support 库时，上面同样的主题必须被替代成如下：

res/values/themes.xml


```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- 应用于程序或者活动的主题 -->
    <style name="CustomActionBarTheme"
           parent="@style/Theme.AppCompat.Light.DarkActionBar">
        <item name="android:actionBarStyle">@style/MyActionBar</item>

        <!-- 支持库兼容 -->
        <item name="actionBarStyle">@style/MyActionBar</item>
    </style>

    <!-- ActionBar 样式 -->
    <style name="MyActionBar"
           parent="@style/Widget.AppCompat.Light.ActionBar.Solid.Inverse">
        <item name="android:background">@drawable/actionbar_background</item>

        <!-- 支持库兼容 -->
        <item name="background">@drawable/actionbar_background</item>
    </style>
</resources>
```

然后，将主题应用到 app 全局或单个的 activity 之中：

<application android:theme="@style/CustomActionBarTheme" ... />
自定义文本颜色
修改 action bar 中的文本颜色，需要分别设置每个元素的属性：

Action bar 的标题：创建一种自定义样式，并指定 textColor 属性；同时，在自定义的 actionBarStyle 中为 titleTextStyle 属性指定为刚才的自定义样式。
注意：被应用到 titleTextStyle 的自定义样式应该使用 TextAppearance.Holo.Widget.ActionBar.Title 作为父样式。

Action bar tabs：在 activity 主题中重写 actionBarTabTextStyle
Action 按钮：在 activity 主题中重写 actionMenuTextColor
仅支持 Android 3.0 和更高
当仅支持 Android 3.0 和更高时，样式 XML 文件应该是这样的：

res/values/themes.xml


```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- 应用于程序或者活动的主题 -->
    <style name="CustomActionBarTheme"
           parent="@style/Theme.Holo">
        <item name="android:actionBarStyle">@style/MyActionBar</item>
        <item name="android:actionBarTabTextStyle">@style/MyActionBarTabText</item>
        <item name="android:actionMenuTextColor">@color/actionbar_text</item>
    </style>

    <!-- ActionBar 样式 -->
    <style name="MyActionBar"
           parent="@style/Widget.Holo.ActionBar">
        <item name="android:titleTextStyle">@style/MyActionBarTitleText</item>
    </style>

    <!-- ActionBar 标题文本 -->
    <style name="MyActionBarTitleText"
           parent="@style/TextAppearance.Holo.Widget.ActionBar.Title">
        <item name="android:textColor">@color/actionbar_text</item>
    </style>

    <!-- ActionBar Tab标签 文本样式 -->
    <style name="MyActionBarTabText"
           parent="@style/Widget.Holo.ActionBar.TabText">
        <item name="android:textColor">@color/actionbar_text</item>
    </style>
</resources>
```

支持 Android 2.1 和更高
当使用 Support 库时，样式 XML 文件应该是这样的：

res/values/themes.xml


```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- 应用于程序或者活动的主题 -->
    <style name="CustomActionBarTheme"
           parent="@style/Theme.AppCompat">
        <item name="android:actionBarStyle">@style/MyActionBar</item>
        <item name="android:actionBarTabTextStyle">@style/MyActionBarTabText</item>
        <item name="android:actionMenuTextColor">@color/actionbar_text</item>

        <!-- 支持库兼容 -->
        <item name="actionBarStyle">@style/MyActionBar</item>
        <item name="actionBarTabTextStyle">@style/MyActionBarTabText</item>
        <item name="actionMenuTextColor">@color/actionbar_text</item>
    </style>

    <!-- ActionBar 样式 -->
    <style name="MyActionBar"
           parent="@style/Widget.AppCompat.ActionBar">
        <item name="android:titleTextStyle">@style/MyActionBarTitleText</item>

        <!-- 支持库兼容 -->
        <item name="titleTextStyle">@style/MyActionBarTitleText</item>
    </style>

    <!-- ActionBar 标题文本 -->
    <style name="MyActionBarTitleText"
           parent="@style/TextAppearance.AppCompat.Widget.ActionBar.Title">
        <item name="android:textColor">@color/actionbar_text</item>
        <!-- 文本颜色属性textColor是可以配合支持库向后兼容的 -->
    </style>

    <!-- ActionBar Tab标签文本样式 -->
    <style name="MyActionBarTabText"
           parent="@style/Widget.AppCompat.ActionBar.TabText">
        <item name="android:textColor">@color/actionbar_text</item>
        <!-- 文本颜色属性textColor是可以配合支持库向后兼容的 -->
    </style>
</resources>
```

 **自定义 Tab Indicator** 

为 activity 创建一个自定义主题，通过重写 actionBarTabStyle 属性来改变 navigation tabs 使用的指示器。actionBarTabStyle 属性指向另一个样式资源；在该样式资源里，通过指定一个state-list drawable 来重写 background 属性。

注意：一个state-list drawable 是重要的，它可以通过不同的背景来指出当前选择的 tab 与其他 tab 的区别。更多关于如何创建一个 drawable 资源来处理多个按钮状态，请阅读 State List 文档。

例如，这是一个状态列表 drawable，为一个 action bar tab 的多种不同状态分别指定背景图片：

res/drawable/actionbar_tab_indicator.xml


```
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">

<!-- 按钮没有按下的状态 -->

    <!-- 没有焦点的状态 -->
    <item android:state_focused="false" android:state_selected="false"
          android:state_pressed="false"
          android:drawable="@drawable/tab_unselected" />
    <item android:state_focused="false" android:state_selected="true"
          android:state_pressed="false"
          android:drawable="@drawable/tab_selected" />

    <!-- 有焦点的状态 (例如D-Pad控制或者鼠标经过) -->
    <item android:state_focused="true" android:state_selected="false"
          android:state_pressed="false"
          android:drawable="@drawable/tab_unselected_focused" />
    <item android:state_focused="true" android:state_selected="true"
          android:state_pressed="false"
          android:drawable="@drawable/tab_selected_focused" />

<!--  按钮按下的状态D -->

    <!-- 没有焦点的状态 -->
    <item android:state_focused="false" android:state_selected="false"
          android:state_pressed="true"
          android:drawable="@drawable/tab_unselected_pressed" />
    <item android:state_focused="false" android:state_selected="true"
        android:state_pressed="true"
        android:drawable="@drawable/tab_selected_pressed" />

    <!--有焦点的状态 (例如D-Pad控制或者鼠标经过)-->
    <item android:state_focused="true" android:state_selected="false"
          android:state_pressed="true"
          android:drawable="@drawable/tab_unselected_pressed" />
    <item android:state_focused="true" android:state_selected="true"
          android:state_pressed="true"
          android:drawable="@drawable/tab_selected_pressed" />
</selector>
```

仅支持 Android 3.0 和更高
当仅支持 Android 3.0 和更高时，样式 XML 文件应该是这样的：

res/values/themes.xml


```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- 应用于程序或活动的主题 -->
    <style name="CustomActionBarTheme"
           parent="@style/Theme.Holo">
        <item name="android:actionBarTabStyle">@style/MyActionBarTabs</item>
    </style>

    <!-- ActionBar tabs 标签样式 -->
    <style name="MyActionBarTabs"
           parent="@style/Widget.Holo.ActionBar.TabView">
        <!-- 标签指示器 -->
        <item name="android:background">@drawable/actionbar_tab_indicator</item>
    </style>
</resources>
```

支持 Android 2.1 和更高
当使用 Support 库时，样式 XML 文件应该是这样的：

res/values/themes.xml

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
     <!-- 应用于程序或活动的主题 -->
     <style name="CustomActionBarTheme"
            parent="@style/Theme.AppCompat">
         <item name="android:actionBarTabStyle">@style/MyActionBarTabs</item>
 
         <!-- 支持库兼容 -->
         <item name="actionBarTabStyle">@style/MyActionBarTabs</item>
     </style>
 
     <!-- ActionBar tabs 样式 -->
     <style name="MyActionBarTabs"
            parent="@style/Widget.AppCompat.ActionBar.TabView">
         <!-- 标签指示器 -->
         <item name="android:background">@drawable/actionbar_tab_indicator</item>
 
         <!-- 支持库兼容 -->
         <item name="background">@drawable/actionbar_tab_indicator</item>
     </style>
 </resources>
```
## ActionBar的覆盖层叠

默认情况下，action bar 显示在 activity 窗口的顶部，会稍微地减少其他布局的有效空间。如果在用户交互过程中要隐藏和显示 action bar，可以通过调用 ActionBar 中的 hide()和show()来实现。但是，这将导致 activity 基于新尺寸重新计算与绘制布局。

为避免在 action bar 隐藏和显示过程中调整布局的大小，可以为 action bar 启用叠加模式(overlay mode)。在叠加模式下，所有可用的空间都会被用来布局就像ActionBar不存在一样，并且 action bar 会叠加在布局之上。这样布局顶部就会有点被遮挡，但当 action bar 隐藏或显示时，系统不再需要调整布局而是无缝过渡。

Note：如果希望 action bar 下面的布局部分可见，可以创建一个背景部分透明的自定义式样的 action bar，如图 1 所示。关于如何定义 action bar 的背景，请查看 自定义ActionBar的风格。

 **启用叠加模式(Overlay Mode)** 

要为 action bar 启用叠加模式，需要自定义一个主题，该主题继承于已经存在的 action bar 主题，并设置 android:windowActionBarOverlay 属性的值为 true。

仅支持 Android 3.0 和以上
如果 minSdkVersion 为 11 或更高，自定义主题必须继承 Theme.Holo 主题（或者其子主题）。例如：


```
<resources>
    <!-- 为程序或者活动应用的主题样式 -->
    <style name="CustomActionBarTheme"
           parent="@android:style/Theme.Holo">
        <item name="android:windowActionBarOverlay">true</item>
    </style>
</resources>
```

支持 Android 2.1 和更高
如果为了兼容运行在 Android 3.0 以下版本的设备而使用了 Support 库，自定义主题必须继承 Theme.AppCompat 主题（或者其子主题）。例如：


```
<resources>
    <!-- 为程序或者活动应用的主题样式 -->
    <style name="CustomActionBarTheme"
           parent="@android:style/Theme.AppCompat">
        <item name="android:windowActionBarOverlay">true</item>

        <!-- 兼容支持库 -->
        <item name="windowActionBarOverlay">true</item>
    </style>
</resources>
```

注意，该主题包含两种不同的 windowActionBarOverlay 式样定义：一个带 android: 前缀，另一个不带。带前缀的适用于包含该式样的 Android 系统版本，不带前缀的适用于通过从 Support 库中读取式样的旧版本。

指定布局的顶部边距
当 action bar 启用叠加模式时，它可能会遮挡住本应保持可见状态的布局。为了确保这些布局始终位于 action bar 下部，可以使用 actionBarSize 属性来指定顶部margin或padding的高度来到达。例如：

```
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingTop="?android:attr/actionBarSize">
    ...
</RelativeLayout>
```

如果在 action bar 中使用 Support 库，需要移除 android: 前缀。例如：

<!-- 兼容支持库 -->
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingTop="?attr/actionBarSize">
    ...
</RelativeLayout>
在这种情况下，不带前缀的 ?attr/actionBarSize 适用于包括Android 3.0 和更高的所有版本。

# 管理Activity的生命周期

当用户进入，退出，回到我们的App时，程序中的Activity 实例会在生命周期中的不同状态间进行切换。例如，activity第一次启动的时候，它来到系统的前台，开始接受用户的焦点。在此期间，Android系统调用了一系列的生命周期中的方法。如果用户执行了启动另一个activity或者切换到另一个app(此时虽然当前activity不可见，但其实例与数据仍然存在)的操作, 系统又会调用一些生命周期中的方法。

在生命周期的回调方法中，可以声明当用户离开或者重新进入这个Activity所需要执行的操作。例如, 如果我们建立了一个streaming video player, 在用户切换到另外一个app的时候，应该暂停video 并终止网络连接。当用户返回时，我们可以重新建立网络连接并允许用户从同样的位置恢复播放。

本章会介绍一些Activity生命周期中重要的回调方法，如何使用那些方法，使得程序符合用户的期望且在activity不需要的时候不会导致系统资源的浪费。

## 启动与销毁Activity

不同于其他编程范式（程序从main()方法开始启动），Android系统根据生命周期的不同阶段唤起对应的回调函数来执行代码。系统存在启动与销毁一个activity的一套有序的回调函数。

本课介绍生命周期中最重要的回调函数，并演示如何处理启动一个activity所涉及到的回调函数。

理解生命周期的回调
在一个activity的生命周期中，系统会像金字塔模型一样去调用一系列的生命周期回调函数。Activity生命周期的每一个阶段就像金字塔中的台阶。当系统创建了一个新的activity实例，每一个回调函数会向上一阶移动activity状态。处在金字塔顶端意味着当前activity处在前台并处于用户可与其进行交互的状态。

当用户退出这个activity时，为了回收该activity，系统会调用其它方法来向下一阶移动activity状态。在某些情况下，activity会隐藏在金字塔下等待(例如当用户切换到其他app),此时activity可以重新回到顶端(如果用户回到这个activity)并恢复用户离开时的状态。

basic-lifecycle

Figure 1. 下面这张图讲解了activity的生命周期：(这个金字塔模型要比之前Dev Guide里面的生命周期图更加容易理解，更加形象)

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-Activity1.png)
根据activity的复杂度，也许不需要实现所有的生命周期方法。但了解每一个方法的回调时机并在其中填充相应功能，使得确保app能够像用户期望的那样执行是很有必要的。如何实现一个符合用户期待的app，我们需要注意下面几点：


- 使用app的时候，不会因为有来电通话或者切换到其他app而导致程序crash。
- 用户没有激活某个组件时不会消耗宝贵的系统资源。
- 离开app并且一段时间后返回，不会丢失用户的使用进度。
- 设备发生屏幕旋转时不会crash或者丢失用户的使用进度。

下面的课程会介绍上图所示的几个生命状态。然而，其中只有三个状态是静态的，这三个状态下activity可以存在一段比较长的时间。(其它几个状态会很快就切换掉，停留的时间比较短暂)

- Resumed：该状态下，activity处在前台，用户可以与它进行交互。(通常也被理解为"running" 状态)
- Paused：该状态下，activity的部分被另外一个activity所遮盖：另外的activity来到前台，但是半透明的，不会覆盖整个屏幕。被暂停的activity不再接受用户的输入且不再执行任何代码。
- Stopped：该状态下, activity完全被隐藏，对用户不可见。可以认为是在后台。当stopped, activity实例与它的所有状态信息（如成员变量等）都会被保留，但activity不能执行任何代码。
- 其它状态 (Created与Started)都是短暂的，系统快速的执行那些回调函数并通过执行下一阶段的回调函数移动到下一个状态。也就是说，在系统调用onCreate(), 之后会迅速调用onStart(), 之后再迅速执行onResume()。以上就是基本的activity生命周期。
- 

 **指定程序首次启动的Activity** 

当用户从主界面点击程序图标时，系统会调用app中被声明为"launcher" (or "main") activity中的onCreate()方法。这个Activity被用来当作程序的主要进入点。

我们可以在AndroidManifest.xml中定义作为主activity的activity。

这个main activity必须在manifest使用包括 MAIN action 与 LAUNCHER category 的<intent-filter>标签来声明。例如：


```
<activity android:name=".MainActivity" android:label="@string/app_name">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity>
```

Note:当你使用Android SDK工具来创建Android工程时，工程中就包含了一个默认的声明有这个filter的activity类。

如果程序中没有声明了MAIN action 或者LAUNCHER category的activity，那么在设备的主界面列表里面不会呈现app图标。

 **创建一个新的实例** 

大多数app包括多个activity，使用户可以执行不同的动作。不论这个activity是当用户点击应用图标创建的main activtiy还是为了响应用户行为而创建的其他activity，系统都会调用新activity实例中的onCreate()方法。

我们必须实现onCreate()方法来执行程序启动所需要的基本逻辑。例如可以在onCreate()方法中定义UI以及实例化类成员变量。

例如：下面的onCreate()方法演示了为了建立一个activity所需要的一些基础操作。如声明UI元素，定义成员变量，配置UI等。(onCreate里面尽量少做事情，避免程序启动太久都看不到界面)

TextView mTextView; // Member variable for text view in the layout


```
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Set the user interface layout for this Activity
    // The layout file is defined in the project res/layout/main_activity.xml file
    setContentView(R.layout.main_activity);

    // Initialize member TextView so we can manipulate it later
    mTextView = (TextView) findViewById(R.id.text_message);

    // Make sure we're running on Honeycomb or higher to use ActionBar APIs
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        // For the main activity, make sure the app icon in the action bar
        // does not behave as a button
        ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(false);
    }
}
```

Caution：用SDK_INT来避免旧的系统调用了只在Android 2.0（API level 5）或者更新的系统可用的方法（上述if条件中的代码）。旧的系统调用了这些方法会抛出一个运行时异常。

一旦onCreate 操作完成，系统会迅速调用onStart() 与onResume()方法。我们的activity不会在Created或者Started状态停留。技术上来说, activity在onStart()被调用后开始被用户可见，但是 onResume()会迅速被执行使得activity停留在Resumed状态，直到一些因素发生变化才会改变这个状态。例如接收到一个来电，用户切换到另外一个activity，或者是设备屏幕关闭。

在后面的课程中，我们将看到其他方法是如何使用的，onStart() 与 onResume()在用户从Paused或Stopped状态中恢复的时候非常有用。

Note: onCreate() 方法包含了一个参数叫做savedInstanceState，这将会在后面的课程 - 重新创建activity涉及到。

basic_lifecycle-create

![输入图片说明](img/Andriod%E5%85%A5%E9%97%A8%E5%9F%BA%E7%A1%80/Andriod-Activity2.png)

Figure 2. 上图显示了onCreate(), onStart() 和 onResume()是如何执行的。当这三个顺序执行的回调函数完成后，activity会到达Resumed状态。

 **销毁Activity** 

activity的第一个生命周期回调函数是 onCreate(),它最后一个回调是onDestroy().当收到需要将该activity彻底移除的信号时，系统会调用这个方法。

大多数 app并不需要实现这个方法，因为局部类的references会随着activity的销毁而销毁，并且我们的activity应该在onPause()与onStop()中执行清除activity资源的操作。然而，如果activity含有在onCreate调用时创建的后台线程，或者是其他有可能导致内存泄漏的资源，则应该在OnDestroy()时进行资源清理，杀死后台线程。

@Override
public void onDestroy() {
    super.onDestroy();  // Always call the superclass

    // Stop method tracing that the activity started during onCreate()
    android.os.Debug.stopMethodTracing();
}
Note: 除非程序在onCreate()方法里面就调用了finish()方法，系统通常是在执行了onPause()与onStop() 之后再调用onDestroy() 。在某些情况下，例如我们的activity只是做了一个临时的逻辑跳转的功能，它只是用来决定跳转到哪一个activity，这样的话，需要在onCreate里面调用finish方法，这样系统会直接调用onDestory，跳过生命周期中的其他方法。

## 暂停与恢复Activity

## 停止与重启Activity

## 重新创建Activity

# 使用Fragment建立动态的UI

## 创建一个fragment

## 构建有弹性的UI

## 与其他fragments交互

# 数据保存

## 创建一个Fragment

## 建立灵活动态的UI

## Fragments之间的交互

# 数据保存
