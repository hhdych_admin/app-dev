# Android应用开发快速入门

> Android 是一个基于 Linux 的移动设备操作系统，主要使用于移动终端设备，比如智能手机、平板电脑、点餐机、送餐机器人等。Android 系统下应用开发语言可以采用 Java 和 Kotlin 两种。企业中 Java 语言应用的比较广泛，另外大学期间许多高校都开设了 Java 编程课程，所以我们这里以 Java 语言为主来介绍 Android 应用开发。

![image-20220105104803837](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220105104803837.png)

### Java 开发环境与环境变量

> 用 Java 语言学习 Android 应用开发，首先需要安装 Java 的开发环境。下载 JDK（开发工具包）进行安装即可，安装 JDK 的同时会提醒安装 JRE（运行时环境）也一并安装。安装完了 JDK 和 JRE 之后，找到安装目录到系统设置中配置环境变量就可以使用 Java 语言进行开发了。

#### 下载 JDK

> 本文采用 JDK 8 为工具集进行讲解。[点击在Oracle官网下载 JDK 8](https://download.oracle.com/otn/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-windows-x64.exe?AuthParam=1641724553_5999ac5721ed3bee978b49f4f4aebccd) ，需要注意的是：Oracle官网对JDK的下载进行了一些限制，需要登录且接受cookie才可以下载。为了简化下载的操作，可以从gitee仓库中下载，详细操作步骤如下：

1. 下载JDK的压缩包分区文件：

   * [分区1](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/asserts/jdk-8u191-windows-x64.zip.001) 
   * [分区2](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/asserts/jdk-8u191-windows-x64.zip.002) 
   * [分区3](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/asserts/jdk-8u191-windows-x64.zip.003) 

2. 下载完成后会得到三个文件，文件信息如下：

   ![image-20220118121128077](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220118121128077.png)
   
3. 由于gitee仓库对单个文件大小有限制，大于100M无法上传，所以将JDK文件压缩成3个分区文件。以上3个文件下载完成后，关于解压需要注意按照如下步骤进行：

   1. 将3个文件放在同一个目录下；

   2. 在第一个文件 `jdk-8u191-windows-x64.zip.001` 上右键进行解压，如下图所示：

      <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220118121930291.png" alt="image-20220118121930291" style="zoom:50%;" />

   3. 解压完成后，即可在当前目录中发现一个新的文件夹 `jdk-8u191-windows-x64` ，文件夹内的文件就是JDK的安装文件 `jdk-8u191-windows-x64.exe` .

   ps：本教程采用的解压缩文件是 7-zip ，如使用的是其他的解压缩软件，同理也是解压第一个文件即可。

#### 安装 JDK

> 只需按照提示进行下一步安装即可，安装目录最好选择全英文目录。需要注意的是安装过程中会提示安装 JRE ，按照提示点击确定安装即可。

#### 配置环境变量

> 安装完成之后，需要在系统的环境变量中进行配置以后才可以再命令行中使用 Java。

操作步骤：控制面板 -> 系统 -> 关于 -> 高级系统设置 -> 环境变量，打开环境变量窗口后，在系统变量中新建系统变量，内容如下：

![image-20220106142848986](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106142848986.png)

测试是否配置成功，同时按下 `Win + R` 输入 `cmd` ，然后敲回车，这时会打开一个命令行，在命令行中输入 `java -version` 命令，显示如下即配置成功。

![image-20220106143755213](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220106143755213.png)

### Java SE 8 组件结构

> Java 平台标准版（Java SE）8包含两块：Java SE 开发工具包（JDK）8、Java SE 运行时环境（JRE）8。JDK 8 包含了 JRE 8 中的所有内容，以及开发应用程序所需要的编译器和调试器等工具。JRE 8 提供了库、Java 虚拟机（JVM）和其他组件来运行用 Java 语言编写的程序，下图是Java SE的产品组件：

![image-20220114114113297](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114114113297.png)

### Java 语言的特点

> Java语言的核心思想是面向对象编程，可以说一个 Java 的程序其实就是一系列对象的集合，通过对这些对象互相调用来完成协同工作。具体特点如下：

* 跨平台：Java 语言中有一个自带的虚拟机 JVM，Java 程序经过编译后生成的字节码是与平台无关的，可以被JVM所识别，只需在各个系统平台中安装Java运行环境即可运行Java程序。
* 面向对象：Java 是一种面向对象的语言，所有的物体均可以被描述为对象，将对象的属性、方法等信息封装起来就是 Java 中的类。非常易于理解和应用。
* 多线程：Java 内置对多线程的支持，允许同时完成多个任务。
* 等等……

### 构建首个Android应用

> 准备好了 JDK 环境以后，就可以使用Android官方推荐的开发工具Android Studio进行Android程序的开发了。

#### Android Studio下载安装

> Android Studio有两种下载方式，一种是exe格式的可执行文件，另一种是zip压缩包文件。
>
> * [android-studio-2020.3.1.26-windows.zip](https://r4---sn-2x3edn7l.gvt1.com/edgedl/android/studio/ide-zips/2020.3.1.26/android-studio-2020.3.1.26-windows.zip) 
> * [android-studio-2020.3.1.26-windows.exe](https://r3---sn-2x3edn7e.gvt1.com/edgedl/android/studio/install/2020.3.1.26/android-studio-2020.3.1.26-windows.exe) 
>
> 以上二选一下载安装/解压即可。

#### 配置Android Studio

> Android Studio安装成功后，目录结构如下图所示：

![image-20220114142956824](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114142956824.png)

> 双击打开 `bin` 目录下的 `studio64.exe` 文件，启动程序界面显示如下：

![image-20220114144032468](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114144032468.png)

> 第一次运行的时候会提示无法访问 `Android SDK` 的附加组件列表，由于 `Andoid SDK` 的相关组件资源是在国外的服务器中，在国内一般是访问不了的，所以Andoid官方建议设置代理后再下载。在中国提供或者使用VPN属于违法行为，所以本教程内无法提供代理的搭建与配置等相关信息，如需代理可自行百度。直接点击 `Cancel` 取消，进入下一界面，如下：

![image-20220114144532875](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114144532875.png)

> 这里提示我们找不到 `Android SDK` ，点击 `Next` 进入下一步，如下：

![image-20220114151029121](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114151029121.png)

> 接下来安装 `Android SDK` ，只需按照上图中的红色字体提示进行操作即可，完成后点击 `Next` 进入下一步，界面如下：

![image-20220114151229631](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114151229631.png)

> 确认当前设置以后，点击 `Finish` ，进入下载页面，等待下载完成（这一步如发现进度卡住不动，下方提示 `timeout` 等字眼即表示连接超时，需要检查网络，如网络没问题那就需要代理才能够完成下载）。

![image-20220114151332600](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114151332600.png)

> 组件下载完成后，显示界面如下，点击 `Finish` 完成即可。

![image-20220114151752313](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114151752313.png)

#### 创建Android项目

> 配置完Android Studio后，就进入到了Android项目的创建。在上面配置Android Studio中的最后一步下载SDK完成后的界面中，点击 `Finish` 即可进入到创建Android项目的界面（如未打开下面界面，重新双击Android Studio安装目录下的 `bin/studio64.exe` 文件即可），如下图：

![image-20220114154506632](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114154506632.png)

> 点击上图中的蓝色按钮(New Project)，进入到创建项目界面，如下图：

![image-20220114155403122](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114155403122.png)

> 按照上图中提示进行操作，然后点击 `Next` 进入下一步：

![image-20220114155923964](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114155923964.png)

> 配置项目的相关信息，各个字段说明如下：
>
> * **Name**：定义程序的名称（建议为英文）
> * **Package name**：定义程序的包名（包名是应用程序的标识符，用于在手机中识别自己的程序，不可与其他应用程序重复否则安装程序时会出错，建议设置多级包名，每一级之间用英文 `.` 隔开即可）
> * **Save location**：项目的保存路径（最好为英文，中文可能会出现乱码等问题）
> * **Language**：开发语言，这里选择Java即可；
> * **Minimum SDK**：程序支持最小的手机版本（默认值即可）；
>
> 上述配置完成后，点击 `Finish` 进入Android Studio的主窗口，开始项目创建和载入。

![image-20220114161309371](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114161309371.png)

> 第一次创建项目Android Studio会做一些初始化的索引、Gradle依赖下载等操作，其中Gradle下载需要点时间，因为访问的是国外资源，如果没有代理可能会下载失败。下载完成后，显示界面如下：

![image-20220114164601525](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114164601525.png)

> 上图中整个界面可分为两块，左侧红色框体部分是项目的目录结构，右侧蓝色框体部分为代码编辑区。
>
> 至此，第一个Android项目创建成功。

#### Android项目目录结构

> Android项目的目录中文件较多，我们来了解一下最重要的几个文件。首先将项目目录结构窗体切换为 `Project` 窗口模式，操作如下：

![image-20220114165153627](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114165153627.png)

> 进入 `Project` 窗口模式后，显示如下：

![image-20220114170512604](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114170512604.png)

> 几个重要文件的说明如下：

* **app > src > main > java > com.itheima.firstapplication > MainActivity** 

  > 应用程序的入口，系统会在这个文件中加载布局（界面）文件；

* **app > src > main > res > layout > activity_main.xml** 

  > 布局文件，此文件定义了activity的界面布局。默认里边有一个TextView文本控件，该控件显示“Hello，World！”文本信息；

* **app > src > main > AndroidManifest.xml** 

  > 应用程序的清单文件，其中定义了应用的基本特性和各个组件；

* **app > build.gradle** 

  > 应用程序的Gradle配置文件，该文件控制应用的构建方式；

* **build.gradle** 

  > 项目的Gradle配置文件，该文件控制项目下所有应用（项目中可创建多个应用）的构建信息。

#### 运行Android程序

> 上面我们已经创建了一个Android应用程序，接下来我们可以再手机或者模拟器上运行它。

##### 在手机上运行

> 在手机上运行，需要先将手机进行相关的配置，步骤如下：

1. 使用USB线将手机和电脑**连接**在一起；
2. **安装**USB驱动程序；
   * 使用360手机助手安装，下载并安装360手机助手以后，当手机和电脑第一次连接后，360手机助手即提示安装驱动，按照指示一步步操作即可；
   * 根据手机型号下载对应的USB驱动程序并进行安装（因手机型号众多，操作方法也不同无法一一枚举，这里略）；
3. 设置手机启用**USB调试**功能；
   * 打开手机中的**设置**；
   * 如果手机的系统版本是Android v8.0及以上，选择**系统**，否则请看下一步；
   * 滑动到底部，选择**关于手机**；
   * 滑动到底部，连续点击**版本号**七次，下方会弹出一个通知：切换到开发者模式；
   * 返回上一屏幕，滚动到底部，点击**开发者选项**；
   * 在开发者选项中找到并**启用USB调试**。

> 手机配置好以后，即可在Android Studio主界面中选择手机，并将程序发布到手机上运行，操作如下图：

![image-20220114172702169](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114172702169.png)

> 按照上图的1、2、3步骤，分别点击后，Gradle就会进入构建任务，等待构建任务完成后，手机会提示安装软件，如下图：

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114174756066.png" alt="image-20220114174756066" style="zoom: 25%;" />  <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114174821520.png" alt="image-20220114174821520" style="zoom: 25%;" /> <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114174844807.png" alt="image-20220114174844807" style="zoom: 25%;" />

> 连续点击两次继续安装后，软件即可安装成功。程序启动后在屏幕中间显示HelloWorld文本字样。
>
> ps：本教程采用的是华为手机且已升级为HarmonyOS（鸿蒙）系统，在运行程序时提示下列错误信息：
>
> ```
> Failed to install the following Android SDK packages as some licences have not been accepted.
> 
>   build-tools;28.0.2 Android SDK Build-Tools 28.0.2
> 
> To build this project, accept the SDK license agreements and install the missing components using the Android Studio SDK Manager.
> 
> Alternatively, to transfer the license agreements from one workstation to another, see http://d.android.com/r/studio-ui/export-licenses.html
> 
> Using Android SDK: C:\AndroidSDK
> ```
>
> 信息显示未获得 `SDK license` 的同意，问题解决办法如下：
>
> 1. 同时按下 `Win + R` 键打开命令行；
> 2. 进入到 `Android SDK` 的目录下找到 `tools/bin` 目录；
> 3. 执行 `sdkmanager.bat --licenses` 命令；
> 4. 命令执行后，会开始重新审查所有的 `licenses` ，审查过程中确认是否同意（会有多次），只需输入字母 `y` ，然后敲回车即可。
>
> 所有的 `SDK licenses` 授权成功后，再回到Android Studio中运行程序即可。Android Studio在将应用程序运行到手机上后会在主界面的下方中间位置显示一条绿色的消息，内容为 `Launch succeeded` ，即表示程序在手机上运行成功。

![image-20220114174530984](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114174530984.png)

##### 在模拟器上运行

> 如果没有真实手机来作为调试工具，可采用Android Studio中自带的 `AVD Manager` 创建一个手机模拟器作为调试工具。

###### 创建模拟器

1. 依次选择 `Tool > AVD Manager` ;

2. 点击 `Create Virtual Device` ；

3. 从左到右依次选择 `Phone > Galaxy Nexus > Next` ；

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114182936638.png" alt="image-20220114182936638" style="zoom:50%;" />

4. 点击镜像列表中的第一行 `Download` 进入下载界面；

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114183114698.png" alt="image-20220114183114698" style="zoom:50%;" />

5. 下载完成后点击 `Finihs` 回到上级页面，点击 `Next` 进入配置页面；

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114184650204.png" alt="image-20220114184650204" style="zoom:50%;" />

6. 配置完成后点击 `Finish` 进入虚拟机列表页面，启动刚刚创建的虚拟机；

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220114184754345.png" alt="image-20220114184754345" style="zoom:50%;" />

7. 启动成功后，界面如下：

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115095652010.png" alt="image-20220115095652010" style="zoom:80%;" />

###### 使用模拟器运行程序

> 模拟器创建成功并启动以后，即可在Android Studio主界面选择该模拟器运行程序，如下图：

![image-20220115101957708](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115101957708.png)

> 等待Gradle编译完毕后，程序成功运行在模拟器中。

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115102249802.png" alt="image-20220115102249802" style="zoom:80%;" />

### 构建简单的界面

> 掌握了Android Studio的项目创建和程序发布运行以后，接下来我们进入界面的设计。本次界面设计效果如下：

![img](https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/screenshot-activity1.png)

> Android应用界面是以布局和控件组合构建而成，布局是 `ViewGroup` 对象，是控制控件及子布局的容器，控件是 `View` 对象，是一些按钮、文本框、输入框等组件元素。布局与控件层次结构图如下：

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/viewgroup_2x.png" alt="img" style="zoom:50%;" />

> Android提供了布局和控件的 `XML` 声明，所以界面的大部分设计均可在 `XML` 文件中完成。接下来我们进入到Android Studio主界面，开始设计界面。

1. 在项目目录窗口切换到 `Project` 模式，找到 **app > src > main > res > layout > activity_main.xml** 布局文件；

2. 调整主界面窗口，参考下图提示 `1` 隐藏 `Project` 窗口，提示 `2` 切换布局编辑器到 `Split` 模式；

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115113525036.png" alt="image-20220115113525036" style="zoom:50%;" />

   布局界面切换到 `Split` 之后，整个Android Studio的主窗体会分为两部分，如下图。其中左侧红色框体部分为编辑区，右侧蓝色框体部分为预览区。编辑区内有一个布局 `ConstraintLayout` 和一个控件 `TextView` ，可以明显看出，`ConstraintLayout` 布局包含了 `TextView` 控件。

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115125452888.png" alt="image-20220115125452888" style="zoom:50%;" />

3. 删除 `TextView` 控件，在布局 `ConstraintLayout` 内增加一个线性布局，布局的排列方向设置为横向，具体代码如下：

   ```xml
           <LinearLayout
               android:id="@+id/linearLayout"
               android:layout_width="match_parent"
               android:layout_height="wrap_content"
               android:layout_margin="20dp"
               android:orientation="horizontal"
               app:layout_constraintLeft_toLeftOf="parent"
               app:layout_constraintRight_toRightOf="parent"
               app:layout_constraintTop_toTopOf="parent">
   
           </LinearLayout>
   ```

4. 在 `LinearLayout` 布局中依次添加 `EditText` 文本编辑框和 `Button` 按钮，具体代码如下：

   ```xml
           <EditText
               android:id="@+id/edit_text"
               android:layout_width="0dp"
               android:layout_height="wrap_content"
               android:layout_weight="8"
               android:autofillHints="Enter a message" />
   
           <Button
               android:id="@+id/button"
               android:layout_width="0dp"
               android:layout_height="wrap_content"
               android:layout_marginStart="5dp"
               android:layout_weight="2"
               android:text="Send" />
   ```

5. 添加完成后，布局文件整体代码如下：

   ```xml
   <?xml version="1.0" encoding="utf-8"?>
   <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
       xmlns:app="http://schemas.android.com/apk/res-auto"
       xmlns:tools="http://schemas.android.com/tools"
       android:layout_width="match_parent"
       android:layout_height="match_parent"
       tools:context=".MainActivity">
   
       <LinearLayout
           android:id="@+id/linearLayout"
           android:layout_width="match_parent"
           android:layout_height="wrap_content"
           android:layout_margin="20dp"
           android:orientation="horizontal"
           app:layout_constraintLeft_toLeftOf="parent"
           app:layout_constraintRight_toRightOf="parent"
           app:layout_constraintTop_toTopOf="parent">
   
           <EditText
               android:id="@+id/edit_text"
               android:layout_width="0dp"
               android:layout_height="wrap_content"
               android:layout_weight="8"
               android:autofillHints="Enter a message" />
   
           <Button
               android:id="@+id/button"
               android:layout_width="0dp"
               android:layout_height="wrap_content"
               android:layout_marginStart="5dp"
               android:layout_weight="3"
               android:text="Send" />
   
       </LinearLayout>
   </androidx.constraintlayout.widget.ConstraintLayout>
   ```

6. 布局编写无误后，点击运行按钮将程序发布运行到模拟器中，模拟器显示如下：

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115141710349.png" alt="image-20220115141710349" style="zoom:80%;" />

### 启动新的界面

> 上面我们做了一个简单的界面，但现在应用程序还是只有一个界面的程序，接下来我们通过刚刚做的界面中的 `SEND` 按钮跳转到一个新的界面中。在Android中每一个界面都是采用Activity来表示，所以我们需要新建一个界面（Activity），然后在 `SEND`按钮被点击时跳转到这个新的界面（Activity）。

#### 监听按钮的点击事件

> 在Android中每一个布局或者控件均有事件的监听机制，我们可以通过对 `SEND` 按钮添加监听事件来捕获用户点击按钮的动作，当用户点击按钮时，Android系统的监听机制将会回调我们刚刚添加的监听函数。

1. 找到 **app > src > main > java > com.itheima.firstapplication > MainActivity**  文件，在 `onCreate` 函数中找到 `SEND` 按钮控件并用对象接收，具体代码如下：

   ```java
   @Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_main);
   
       // 找到布局文件中的SEND按钮控件，命名为button
       Button button = findViewById(R.id.button);	// 控件的ID：button，在前面的布局文件中已添加过
   }
   ```

2. 添加监听事件到 `SEND` 按钮中，具体代码如下：

   ```java
   button.setOnClickListener(new View.OnClickListener() {	// 以内部类的方式实现接口
       @Override
       public void onClick(View view) {
   		// 当SEND按钮被点击时，将会回调本函数
           
       }
   });
   ```

3. 当 `SEND` 按钮被点击时，将会回调上面内部类中的 `onClick` 函数。

#### 构建一个Intent

> Android中界面（Activity）之间的跳转以及数据传递是通过Intent来实现的，Intent可以指定跳转到哪一个界面（Activity）以及传递什么样的数据等。接下来我们通过Intent来开启一个新的界面（Activity），并将用户在文本编辑框中输入的数据传递新的界面中。实现步骤如下：

1. 在按钮的回调函数 `onClick` 中找到 `EditText` 文本编辑框对象，并将用户输入的数据取出来，具体代码如下：

   ```java
   @Override
   public void onClick(View view) {
       // 找到布局文件中的文本编辑框控件，命名为editText
       EditText editText = findViewById(R.id.edit_text);   // 控件的ID：edit_text，在前面的布局文件中已添加过
       // 将用户输入在文本编辑框中的数据取出，并消除两边的空格
       String message = editText.getText().toString().trim();
   }
   ```

2. 创建一个Intent来跳转到新的界面（Activity）中，具体代码如下：

   ```java
   @Override
   public void onClick(View view) {
       // 找到布局文件中的文本编辑框控件，命名为editText
       EditText editText = findViewById(R.id.edit_text);   // 控件的ID：edit_text，在前面的布局文件中已添加过
       // 将用户输入在文本编辑框中的数据取出，并消除两边的空格
       String message = editText.getText().toString().trim();
   
       // 创建一个新的界面，界面指向的文件为：DisplayMessageActivity.java
       Intent intent = new Intent(MainActivity.this, DisplayMessageActivity.class);
       // 将用户输入的数据交给Intent
       intent.putExtra("msg", message);
       // 开启界面
       startActivity(intent);
   }
   ```

3. 上述中新的界面 `DisplayMessageActivity.java` 还没有创建，所以程序还不能运行，接下来要开始创建界面。

#### 创建新的界面（Activity）

> Android中Activity用来表示界面，每一个Activity都会对应一个布局，Activity如果想有效被使用还需在 `AndroidManifest.xml` 清单文件中添加声明。使用Android Studio创建Activity会自动帮我们创建对应的 `XML` 布局文件，并且会在清单文件中添加声明，无需手动再添加。Activity创建步骤如下：

1. 在 `Project` 窗口中找到 `app` 文件夹，右键点击 `app` 文件夹，然后依次选择 **New > Activity > Empty Activity** ，详情如下图：

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115150938251.png" alt="image-20220115150938251" style="zoom:80%;" />

2. 配置新的Activity信息如下：

   <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115151209122.png" alt="image-20220115151209122" style="zoom:80%;" />

   相关字段说明：

   * **Activity Name**：和上面Intent指向的文件名称保持一致（无需填写扩展名称）；
   * **Generate a Layout File**：勾选后会帮我们创建一个与Acitivity对应的布局文件（默认选中即可）；
   * **Layout Name**：布局文件的名称（默认即可）；
   * **Launcher Activity**：是否设置为程序启动的第一个界面（不选中）；
   * **Package name**：程序的包名（默认即可）
   * **Source Language**：开发语言（选中Java）
   * **Target Source Set**：源码分组（默认选中main即可）

3. 配置完Activity的信息后，点击 `Finish` 按钮，Android Studio会执行以下三个操作：

   * 在 `com.itheima.firstapplication` 包下创建 `DisplayMessageActivity.java` 文件；
   * 在 **app > src > main > res > layout** 下创建 `activity_display_message.xml` 布局文件；
   * 在 **app > src > main > AndroidManifest.xml** 文件中添加 `activity` 元素；

4. 在 **app > src > main > res > layout > activity_display_message.xml** 文件中添加一个文本控件，具体代码如下：

   ```xml
   <?xml version="1.0" encoding="utf-8"?>
   <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
       xmlns:app="http://schemas.android.com/apk/res-auto"
       xmlns:tools="http://schemas.android.com/tools"
       android:layout_width="match_parent"
       android:layout_height="match_parent"
       tools:context=".DisplayMessageActivity">
   
       <TextView
           android:id="@+id/text_view"
           android:layout_width="match_parent"
           android:layout_height="match_parent"
           android:gravity="center"
           android:textColor="#0099ff"
           android:textSize="28dp" />
   
   </androidx.constraintlayout.widget.ConstraintLayout>
   ```

5. 在 **app > src > main > java > com.itheima.firstapplication > DisplayMessageActivity** 文件中的 `onCreate` 函数中，找到布局中的 `TextView` 文本控件，并将前一个界面（MainActivity）传递过来的 `msg` 接收过来并显示在 `TextView` 文本控件中。具体代码如下：

   ```java
   @Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_display_message);
   
       // 找到布局文件中的TextView
       TextView textView = findViewById(R.id.text_view);   // 控件ID：text_view，已在布局文件中声明过
   
       // 获取跳转过来的Intent
       Intent intent = getIntent();
       // 取出MainActivity传递过来的msg消息
       String message = intent.getStringExtra("msg");
   
       // 将接收过来的消息显示到TextView控件中
       textView.setText(message);
   }
   ```

#### 运行程序

> 以上代码及布局确认编写无误后，即可运行程序检验成功了。效果图如下：

<img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115154104213.png" alt="image-20220115154104213" style="zoom:50%;" /> <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115154157047.png" alt="image-20220115154157047" style="zoom:50%;" /> <img src="https://gitee.com/openatom-university/app-dev/raw/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/img/image-20220115154245313.png" alt="image-20220115154245313" style="zoom:50%;" /> 