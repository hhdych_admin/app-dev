# 课程大纲

### [01-互联网产品开发流程](https://gitee.com/openatom-university/app-dev/tree/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B)

1. [产品的概念](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/01-%E4%BA%A7%E5%93%81%E7%9A%84%E6%A6%82%E5%BF%B5.md) 
2. [产品开发流程](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/02-%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md) 
   * 互联网公司组织结构
   * 产品与开发的区别
   * MVP模型
3. [用户需求挖掘](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/03-%E7%94%A8%E6%88%B7%E9%9C%80%E6%B1%82%E6%8C%96%E6%8E%98.md)
   * 用户痛点
   * 应用场景
     * 伪需求案例分析
     * 六何分析法
     * 案例分析《海岛赤脚》

4. [市场调研分析](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/04-%E5%B8%82%E5%9C%BA%E8%B0%83%E7%A0%94%E5%88%86%E6%9E%90.md)
   * 市场分析
     * 环境&趋势
     * 空间&规模
   * 竞品分析
     * 分析流程
   * 用户分析
     * 定性分析
       定量调研

5. [产品设计与规划](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/05-%E4%BA%A7%E5%93%81%E8%AE%BE%E8%AE%A1%E4%B8%8E%E8%A7%84%E5%88%92.md)
   * 产品定位
   * 需求分析
   * 产品架构
   * 功能结构
   * 信息结构
   * 原型设计
     * 原型设计入门
     * 高保真原型
     * 原型设计实战
   * 功能说明

6. [产品需求评审](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/01-%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B/06-%E4%BA%A7%E5%93%81%E9%9C%80%E6%B1%82%E8%AF%84%E5%AE%A1.md) 
   * 案例分析《水滴_PRD_v1.0》

### [02-Android开发课程](https://gitee.com/openatom-university/app-dev/tree/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B)

1. [移动App开发简介](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/01-%E7%A7%BB%E5%8A%A8App%E5%BC%80%E5%8F%91%E7%AE%80%E4%BB%8B.md)
   * 操作系统
   * 软件结构
     * 服务端（Server）
     * 用户端（Client）
     * 示例
       * 产品功能的规划
       * 登录功能演示

2. [Android 应用开发快速入门](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/02-Android%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8.md)
   * Java 开发环境与环境变量
     * 下载 JDK
     * 安装 JDK
     * 配置环境变量
   * Java SE 8 组件结构
   * Java 语言的特点
   * 构建首个 Android 应用
     * Android Studio 下载安装
     * 配置Android Studio
     * 创建 Android 项目
     * Android 项目目录结构
     * 运行 Android 程序
       * 在手机上运行
       * 在模拟器上运行
         * 创建模拟器
         * 使用模拟器运行程序
   * 构建简单的界面
   * 启动新的界面
     * 监听按钮的点击事件
     * 构建一个 Intent
     * 创建新的界面（Activity）
     * 运行程序
3. [前后端综合案例《用户登录》](https://gitee.com/openatom-university/app-dev/blob/master/%E8%AF%BE%E4%BB%B6/02-Andriod%E5%BC%80%E5%8F%91%E8%AF%BE%E7%A8%8B/03-%E5%89%8D%E5%90%8E%E7%AB%AF%E7%BB%BC%E5%90%88%E6%A1%88%E4%BE%8B%E3%80%8A%E7%94%A8%E6%88%B7%E7%99%BB%E5%BD%95%E3%80%8B.md)
   * 《用户登录》业务流程图
   * 前后端接口定义
   * 后端程序开发
     * 后端程序启动
     * 检验后端程序
   * 前端程序开发
     * 功能列表
     * 编码实现
       * 创建 Moudle
       * 登录界面设计
       * 登录业务逻辑实现
       * 普通/管理员用户界面设计
       * 普通/管理员用户业务逻辑实现
     * 登录功能测试