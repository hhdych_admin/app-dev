# 1. 关于 Gitee 

Gitee 是开源中国社区2013年推出的基于 Git 的代码托管服务，目前已经成为国内最大的代码托管平台，致力于为国内开发者提供优质稳定的托管服务。

# 2. 主要功能

Gitee 除了提供最基础的 Git 代码托管之外，还提供代码在线查看、历史版本查看、Fork、Pull Request、打包下载任意版本、Issue、Wiki 、保护分支、代码质量检测、PaaS项目演示等方便管理、开发、协作、共享的功能。

# 3. 注册个人 Gitee 账号

个人开发者可免费创建 1000 个仓库（不限公有、私有），提供最多 5G 的免费代码存储空间通过访问 https://gitee.com/，从首页右上角点击「注册」或点击「加入 Gitee」即可注册个人账号。在 Gitee 的注册界面依次填入各项，需注意的是：邮箱建议填写国内邮箱如163邮箱/QQ邮箱，以免因为众所周知的原因无法接收激活邮件，个性地址一经选定,无法修改，请慎重填写。当然，你也可以通过微信授权等第三方登录平台授权登录，授权登录后按照引导提示填写相关信息即可完成注册。

![Image description](https://images.gitee.com/uploads/images/2021/0509/221754_5c04e37c_8894319.png "Registration-Gitee.png")

# 4. 配置Gitee平台远程免密SSH公钥

## 4.1 Linux和MacOS配置

### 4.1.1 生成SSH公钥

Gitee 提供了基于SSH协议的Git服务，在使用SSH协议访问仓库仓库之前，需要先配置好账户/仓库的SSH公钥。


```
$ ssh-keygen -t rsa -C "your_email@example.com"
$ cd ~/.shh  
$ cat id_rsa.pub  //显示git客户端的SSH公钥
  ssh-rsa  AAAAB3NzaC1yc2EAAAADAQABAAABgQDKRCMgJFJd45f9S1SNeIN5XHPxM77+i27y4LZ80lZkZEkdxdMk2osBlkV6po59//xDZQIHhDmqXupjcXGOE5Gj1ygM/yWlnSjmMoSwG/yvbds6车gQgJzQ2nv1kJ2uK9jQz2dYI+Ux0ISc2KZBLrSwqgasFoZBMcf+RXoYTqxz7Gn11d/JNnJ07iiZzl7dr66q2AIShDJHW8iWB3OqptT6ajpG+SrZteOC3CE5hJDXglRbQxqSUbDyzMOmuR+qt/pDNnmHJYYXqBA9655WEqYizCHNgJ/g0LH0wseKEZmetFyp48j+erkhvOurLHFG2PZq27CH8jFuDfex9tVvyPH3Uq2aApnnz/gNEAfOyNAIPAAfOogwdKS7hohZgosWymSoib4XbNT2zLUFWt9XQ/Yel8zpuCGFRFRpulA7ztUanAOk57oO3QFKP0Vid/AMworOs5qLZEY/ITZr6jC40FALbih++FxssMc/6Lmriydmn1U2mq36R0clWyiWaWKTOnwRM= guohao@openatom.org
```

### 4.1.2 添加SSH公钥

![Image description](https://images.gitee.com/uploads/images/2021/0520/142729_5cf97037_8894319.png "SSH-keys-Gitee.png")

![Image description](https://images.gitee.com/uploads/images/2021/0520/142749_193fab84_8894319.png "SSH-keys-Gitee3.png")

![Image description](https://images.gitee.com/uploads/images/2021/0520/142821_0bf53684_8894319.png "SSH-keys-Gitee4.png")

## 验证git客户端和gitee SSH通讯是否正常


```
$ ssh -T git@gitee.com
$ Hi gzkoala! You've successfully authenticated, but GITEE.COM does not provide shell access.  //显示验证已经通过
```

备注：Fedora33按照ssh-keygen -rsa生成的Public Key可能会被认为有安全漏洞，所以会出现

```
$ git@gitee.com: Permission denied (publickey).`
```

解决方法一：

```
$ cd ~/.ssh
$ vi config

# Gitee
Host gitee.com 
HostName gitee.com 
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_rsa
PubkeyAcceptedKeyTypes +ssh-rsa      //允许rsa加密算法       
```

解决方案二：

使用其他的加密算法，例如ECDSA 、ED25519

```
$ ssh-keygen -t ed25519 -C "your_email@example.com"
```
## 4.2 Windows配置

### 4.2.1 安装OpenSSH for Windows

32位下载地址：https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/OpenSSH-Win32.zip
64位下载地址：https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/OpenSSH-Win64.zip

32位将文件解压到 C：\Program Files(x86)\OpenSSH
64位将文件解压到 C：\Program Files\OpenSSH

### 4.2.2 生成SSH公钥


```
cd C:\Program Files\OpenSSH\
C:\Program Files\OpenSSH\ssh-keygen -t rsa -C "your_email@example.com"

cd Users\username\.ssh
type id_rsa.pub
```
参考4.1.2将生成的公钥复制到Gitee的配置当中。

# 5. 申请成为开发者

1. 点击链接https://gitee.com/openatom-university/openharmony-oer/invite_link?invite=af1b6b9252a0e9511f208a1ccef785bbe2242e5356643142b7993663152babc501ad49a08a2d5d4f5f318cd36bbddc3a

2. 或扫描二维码

![Image description](https://images.gitee.com/uploads/images/2021/0520/143009_56283cc1_8894319.png "canvas.png")

# 6. 克隆代码仓到本地

```
$ git clone https://gitee.com/openatom-university/openharmony-oer.git
$ cd openharmony-oer
$ git checkout feature        //切换到feature分支
```

# 7. 提交文件

```
$ git add 文件名或者目录名
$ git commit -m "xxx"
$ git push origin feature
```

# 8. 提交issue

1. 点击New Issue

![Image description](https://images.gitee.com/uploads/images/2021/0520/143215_d158b13a_8894319.png "issue1.png")

2. 输入Issue的内容

![Image description](https://images.gitee.com/uploads/images/2021/0520/143243_71ffad62_8894319.png "issue2.png")